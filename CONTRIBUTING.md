# Aspiring Software Designer

## Lorenz Genesis M.Reyes

👋 Aspiring Software Designer! 🚀👨‍💻 — 💌 lorenzgenesisreyes@laverdad.edu.ph — Calumpit, Bulacan

(![Alt text](image.png))

### Bio

**Good to know:** I am the type of person who is confident in doing things that I have mastered, and maybe people don't see it, but I feel hard feelings when I can't understand something, and that thing makes my mood alter every time, but I practice not making others feel bad just because I am in a bad mood. I love physical activities. I love cycling and lifting weights. I also enjoy participating in church work, and it never gets boring to me.

**Motto:** Be good to those people that you love; make them feel how much they are worth to you.

**Languages:** Python, Java, HTML

**Other Technologies:** VS code, PyCharm,

**Personality Type:** [Entertainer (ESFP-A / ESFP-T)]
<!-- END -->
