#TASK_1

num1 = 20
num2 = 7
results = num1 + num2
print("The sum of the two given variable is:", results)

#TASK_2

name = "Lorenz Genesis M.Reyes"
print("Hello", name, "!")

#TASK_3

global_var = 10


def local_function():

  local_var = 5

  print("Local_function:")
  print("global_var =", global_var)
  print("local_var =", local_var)


local_function()

#TASK_4
global_var = 10


def modify_global():

  global global_var
  global_var = 20


modify_global()

print("After calling modify_global, global_var =", global_var)

#TASK_5

# Assign values to num1 and num2
num1 = 10
num2 = 9


def calculate_operations(a, b):
  sum_result = a + b
  difference_result = a - b
  product_result = a * b

  if b != 0:
    quotient_result = a / b
  else:
    quotient_result = "Division by zero is undefined."
  print("Sum:", sum_result)
  print("Difference:", difference_result)
  print("Product:", product_result)
  print("Quotient:", quotient_result)


calculate_operations(num1, num2)

#TASK_6

#area formula : Area = (base * height) / 2
base = 8
height = 6
Area = (base * height) / 2
print("The area of a triangle is:", Area)

#TASK_7

count = 5
count += 3
count *= 2

print("The final value of count is:", count)

#TASK_8  - 20% discount in value of 100

price = 100
price *= 0.80

print("The final price that has discount is:", price)

#TASK_9

x = 96
y = 69

if x > y:
  print("X is greater than Y")

else:
  print("Y is greater than X")

#TASK_10

str1 = "Nigeria"
str2 = "nigeria"

if str1 == str2:
  print("str1 and str2 are equal")

else:
  print("str1 and str2 are not equal")

#TASK_11

fruits = [
    "banana", "orange", "apple", "grape", "kiwi", "tamarind", "star fruit",
    "melon"
]

if "apple" in fruits:
  print("Yes, 'apple' is in the list of fruits.")
else:
  print("No, 'apple' is not in the list of fruits.")

#TASK_12

sentence = "The days into years roll by It is where that I live until I die Ordinary world."

if "world" not in sentence:
  print("The word 'WORLD' is not in the sentence.")
else:
  print("The word 'WORLD' is in the sentence.")

#TASK_13

my_list = []

my_list.append(5)
my_list.append(10)
my_list.append(15)

print(my_list)

#TASK_14

numbers = [1, 2, 3, 4, 5]
numbers.pop(-1)

print(numbers)

#TASK_15

numbers = list(range(1, 11))
sublist = numbers[2:7]

print(sublist)

#TASK_16

sentence = "The quick brown fox jumps over the lazy dog"
word_quick = sentence[4:9]

print(word_quick)

#TASK_17

colors = ["red", "blue", "green", "yellow", "purple", "indigo", "black"]

if "green" in colors:
  index_green = colors.index("green")
  print("The index of 'green' is:", index_green)
else:
  print("'Green' is not in the list of colors.")

#TASK_18

fruit = ["star fruit", "melon", "banana", "papaya", "langka", "grapes"]

if "mango" in fruits:
  index_mango = fruits.index("mango")
  print("The index of 'mango' is:", index_mango)
else:
  print("'mango' is not in the list of fruits.")

#TASK_19

coordinates = (37.7749, -122.4194)

print(coordinates)

#TASK_20


def lorenz_pogi(num1, num2):
  quotient = num1 // num2
  remainder = num1 % num2
  return (quotient, remainder)


result = lorenz_pogi(20, 3)
print("Quotient:", result[0])
print("Remainder:", result[1])

#TASK_21

person_info = {
    "name": "John Cena",
    "age": 48,
    "city": "West Newbury, Massachusetts"
}
print("Person's name:", person_info["name"], ", age", person_info["age"],
      ", city", person_info["city"])

#TASK_22

person_info = {
    "name": "John Cena",
    "age": 48,
    "city": "West Newbury, Massachusetts"
}
person_info["city"] = "Mexico Pampanga"
print("Updated Person's Information:")

print(person_info)

#TASK_23

numbers = list(range(1, 11))

print(numbers)

#TASK_24

even_numbers = list(range(2, 21, 2))

print("list of even numbers:")
print(even_numbers)

#TASK_25

print("Hello, World!")

#|TASK_26

fruits = [
    "apple", "banana", "cherry", "date", "fig", "corn", "tomato", "lansones"
]

print(", ".join(fruits))

#TASK_27

user_name = input("Please enter your name: ")
greeting = f"Hello and a great wonderful night to you, {user_name},! Welcome to our program."

print(greeting)

#TASK_28

num1 = float(input("Enter the first number: "))
num2 = float(input("Enter the second number: "))
product = num1 * num2

print(f"The product of {num1} and {num2} is: {product}")

#TASK_29

favorite_books = [
    "Bible", "1984", "let's Get it straight with KDR",
    "Contoversy Extraordinary", "Vinland saga"
]
number_of_books = len(favorite_books)

print("Number of favorite books:", number_of_books)

#TASK_30

user_input = input("Enter a sentence: ")
num_characters = len(user_input)

print(f"The total numbers of characters in the sentence is: {num_characters}")

#TASK_31

for number in range(1, 21):
  if number % 2 != 0:
    continue

  print(number)

#TASK_32

for number in range(1, 21):
  if number % 2 != 0:
    continue

  print(number)

  if number == 15:

    break

#TASK_33

number = 1

while number <= 10:
  print(number)
  number += 1

#TASK_34

names = ["Alexa", "Baron", "Chris", "Dansen", "Erickson"]
for name in names:

  print(f"Hello, {name}!")

#TASK_35


def add_numbers(num1, num2):
  result = num1 + num2
  return result


num1 = 11
num2 = 7
sum_result = add_numbers(num1, num2)

print(f"The sum of {num1} and {num2} is: {sum_result}")

#TASK_36


def find_max(numbers):
  if not numbers:
    return None

  max_value = numbers[0]

  for num in numbers:
    if num > max_value:
      max_value = num

  return max_value


number_list = [23, 45, 12, 67, 89, 34]
max_number = find_max(number_list)

print(f"The maximum value in the list is: {max_number}")
